from django.shortcuts import render
from django.http import HttpRequest, HttpResponse


def index(request: HttpRequest) -> HttpResponse:
    return HttpResponse('Hello, world!')


def lesson_1_1(request: HttpRequest) -> HttpResponse:
    return render(request, 'index.html', {})
